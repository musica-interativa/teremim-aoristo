"arduino-hcsr04-init.sc".loadRelative

( // instrumento 02: BMoog
z = {
    var sig, freq;
	freq = 300 - (In.kr(~distance.index) * 10);
    sig = BMoog.ar(
        LFSaw.ar([freq * 0.99, freq * 1.01], 0, 0.1) ! 2,
        SinOsc.kr(SinOsc.kr(0.1),1.5pi,1550,1800),
		In.kr(~shake.index) / 100,
        LFSaw.kr(1,0,3));    // filter mode - sweep modes
     (CombN.ar(sig, 0.4, [0.4,0.35],2) * 0.4) + (sig * 0.5);
}.play;
)

z.release;

"arduino-hcsr04-stop.sc".loadRelative
