// theremin aoristo (NANO version)

// loop to read Arduino serial from HCSR-04 sensor
// also, read on/off buttons

(
s.boot;
s.waitForBoot;
~distance = Bus.control(s, 1);
~shake = Bus.control(s, 1);
~port = SerialPort("/dev/ttyUSB0", baudrate: 115200); // crtscts: true);
~loop = Routine({ inf.do{
	d = ~port.read; // distance
	k = ~port.read; // shake
	("distance: " + d).postln;
	("shake: " + k).postln;
	~distance.set(d);
	~shake.set(k);
}}).play;
)

// osciloscopio
s.scope;

// listar portas serial (DEBUG)
SerialPort.listDevices();

( // TODOS os INSTRUMENTOS

// INSTRUMENTO: decimator
(
SynthDef(\decimator, {
	var distance = In.kr(~distance);
	var shake = In.kr(~shake);
	var freq = distance.linlin(0, 35, 700, 500);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = Decimator.ar(
		SinOsc.ar(freq, 0, 0.2),
		5000 / distance,
		shake / 10
    );
	Out.ar(0, Pan2.ar(Vibrato.ar(sound + LFPar.ar(shake / 10), shake / 100, freq / 1000), 0, amp));
}).add;
);

// INSTRUMENTO: bmoog
(
SynthDef(\bmoog, {
	var sig, freq;
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.01, 0.60);
	freq = 350 - (In.kr(~distance.index) * 10);
    sig = BMoog.ar(
        LFSaw.ar([freq * 0.99, freq * 1.01], 0, 0.1) ! 2,
        SinOsc.kr(SinOsc.kr(0.1),1.5pi,1550,1800),
		shake / 100,
        LFSaw.kr(1,0,3));    // filter mode - sweep modes
	Out.ar(0, Pan2.ar((CombN.ar(sig, 0.4, [0.4,0.35],2) * 0.4) + (sig * 0.5), 0, amp))
}).add;
);

// INSTRUMENTO: reed
(
SynthDef(\reed, {
    |out = 0, freq = 440, amp = 0.1, gate = 1, attack = 1, release = 1|
	var snd, blow;
	var shake = In.kr(~shake.index);
	freq = 400 - (In.kr(~distance.index) * 10);
	amp = shake.linlin(0, 100, 0.1, 0.5);
    // pulse with modulating width
    snd = Pulse.ar(freq.cpsmidi, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 1);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    //snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
	Out.ar(out, Vibrato.ar(Pan2.ar(snd, 0, amp), shake, shake / 10));
}).add;
);

// INSTRUMENTO: sine
(
SynthDef(\sine, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 0.4);
	var sound = SinOsc.ar((100 - (distance * 2)) * 2);
	Out.ar(0, Pan2.ar(Vibrato.ar(sound, shake / 10, shake / 10), 0, amp));
}).add;
);

// INSTRUMENTO: saw
(
SynthDef(\saw, {
	var distance = In.kr(~distance.index);
	var freq = (50 - (distance * 2)) * 2;
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 0.9);
	var sound = Saw.ar(freq + shake);
	sound = Vibrato.ar(sound, shake, shake / 100, freq / 100 );
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
);

// INSTRUMENTO: tri
(
SynthDef(\tri, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var freq = (100 - (distance * 2)) * 2;
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = LFTri.ar(freq);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
);

// INSTRUMENTO: varsaw
(
SynthDef(\varsaw, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var freq = (100 - (distance * 2)) * 2;
	var amp = shake.linlin(0, 100, 0.1, 0.8);
	var sound = VarSaw.ar(freq) + SinOsc.ar(Vibrato.kr(shake) * 2);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
);

// INSTRUMENTO: vosim
(
SynthDef(\vosim, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var freq = 3700 - (distance * (5 + shake));
	var amp = shake.linlin(0, 100, 0.1, 0.45);
	var sound = VOSIM.ar(Impulse.ar(100), freq, 3, 0.99) + Vibrato.ar(LFTri.ar(shake));
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
);

// INSTRUMENTO: fatsaw
(
SynthDef(\fatsaw, {
		arg freq=440, amp=0.3, fat=0.0033, ffreq=2000, atk=0.001, dec=0.3, sus=0.5, rls=0.1,gate=1;
		var f1,f2,f3,f4,synth;
	var shake = In.kr(~shake.index);
	var distance = In.kr(~distance.index);
	amp = shake.linlin(0, 100, 0.1, 0.5);
	freq = distance.linlin(0, 35, 220, 30);
	fat = shake / 5000;
	sus = sus * (shake / 25);
	ffreq = distance.linlin(0, 35, 100, 300) * (shake / 10);
		f1=freq-(freq*fat);
		f2=freq-(freq*fat/2);
		f3=freq+(freq*fat/2);
		f4=freq+(freq*fat);
		synth = LFSaw.ar([f1,f2,f3,f4],[0,0.001,0.002,0.004,0.008]);
		synth = synth * EnvGen.kr(Env([0,1,sus,0],[atk,dec,rls],'lin',2),gate,doneAction:0);
		synth=Splay.ar(synth,0.7);
		synth=RLPF.ar(synth,ffreq*Linen.kr(gate,0.1,0.4,0.2,0),0.4);
		Out.ar([0, 1], synth*amp);
  },[0.1,0.3,4,2]).add;
);

// INSTRUMENTO: chaos
(
SynthDef(\chaos, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 0.5);
	var sound = SinOsc.ar(distance + (shake * SinOsc.kr([50, 51], 0, SinOsc.kr(101, Saw.kr(0.12345, 678, 9), 0.2, 0.8), Pulse.kr([25, 25.5], 0.25, 0.125, -0.25))), 0, 0.5, 0);
	Out.ar(0, Pan2.ar(Vibrato.ar(sound + SinOsc.ar(200 - (distance * 5)), shake, shake / 100), 0, amp));
}).add;
);

) // TODOS os INSTRUMENTOS (fim)

~decimator = Synth(\decimator);
~decimator.free;
~bmoog = Synth(\bmoog);
~bmoog.free;
~reed = Synth(\reed);
~reed.free;
~sine = Synth(\sine);
~sine.free;
~saw = Synth(\saw);
~saw.free;
~tri = Synth(\tri);
~tri.free;
~varsaw = Synth(\varsaw);
~varsaw.free;
~vosim = Synth(\vosim);
~vosim.free;
~fastsaw = Synth(\fatsaw);
~fastsaw.free;
~chaos = Synth(\chaos);
~chaos.free;

//////////////////// wip /////////////////////////


// INSTRUMENTO: dfm1
(
z = {
	DFM1.ar(Pulse.ar(120 - In.kr(~distance), mul:0.4) +
		Pulse.ar(120.1 - In.kr(~distance), mul:0.4),
		SinOsc.kr(SinOsc.kr(0.4).range(0.2, In.kr(~shake) / 10)).range(80, 2000), 1.1,
		inputgain: 2.0, mul:0.7)!2
}.play;
)


SynthDef(\hoover, {
    var snd, freq, bw, delay, decay;
    freq = \freq.kr(440);
    freq = freq * Env([-5, 6, 0], [0.1, 1.7], [\lin, -4]).kr.midiratio;
    bw = 1.035;
    snd = { DelayN.ar(Saw.ar(freq * ExpRand(bw, 1 / bw)) + Saw.ar(freq * 0.5 * ExpRand(bw, 1 / bw)), 0.01, Rand(0, 0.01)) }.dup(20);
    snd = (Splay.ar(snd) * 3).atan;
    snd = snd * Env.asr(0.01, 1.0, 1.0).kr(0, \gate.kr(1));
    snd = FreeVerb2.ar(snd[0], snd[1], 0.3, 0.9);
    snd = snd * Env.asr(0, 1.0, 4, 6).kr(2, \gate.kr(1));
    Out.ar(\out.kr(0), snd * \amp.kr(0.1));
}).add;


)

Synth(\hoover)


(
SynthDef(\kalimba, {
    |out = 0, freq = 440, amp = 0.1, mix = 0.1|
    var snd, click;
    // Basic tone is a SinOsc
    snd = SinOsc.ar(freq) * EnvGen.ar(Env.perc(0.03, Rand(3.0, 4.0), 1, -7), doneAction: 2);
	snd = HPF.ar( LPF.ar(snd, 380), 120);
    // The "clicking" sounds are modeled with a bank of resonators excited by enveloped white noise
	click = DynKlank.ar(`[
        // the resonant frequencies are randomized a little to add variation
        // there are two high resonant freqs and one quiet "bass" freq to give it some depth
        [240*ExpRand(0.97, 1.02), 2020*ExpRand(0.97, 1.02), 3151*ExpRand(0.97, 1.02)],
        [-9, 0, -5].dbamp,
        [0.8, 0.07, 0.08]
	], BPF.ar(PinkNoise.ar, 6500, 0.1) * EnvGen.ar(Env.perc(0.001, 0.01))) * 0.1;
	snd = (snd*mix) + (click*(1-mix));
	snd = Mix( snd );
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;
)

(
Pbind(
    \instrument, \kalimba,
    \dur, Pseq([0.3, 0.15], inf),
	\stretch, 1.3,
    \amp, 0.5*(2**Pgauss(0, 0.1)),
    \degree, Pseq([0, -3, [1, 4], 2, Rest, 1, -3, -2, -4, -2, [0, 5], 1, Rest, 0, -2, Rest], inf)
).play;
)


//////////////////////////////////// instrumentos selecionados acima /////////////////////////