  arg freq = 440, modfreq = 1, amp = 0.3, attack = 0.5, dur = 0.2, pos = 0.5;
  var carrier, modulator, env;
  modulator = LFSaw.kr(modfreq).range(0.5, 0.7);
  carrier = LFSaw.ar(freq: freq, mul: modulator);
  env = Env.perc(attackTime: attack, releaseTime: dur - attack, level: amp).kr(2);
  carrier = carrier * env;
  Out.ar(0, Pan2.ar(carrier, pos))