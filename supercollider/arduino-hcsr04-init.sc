/*
sintetizadores e sons para usar no teremim aoristo
http://sccode.org/1-4Vt
http://sccode.org/1-4YY
http://sccode.org/1-4YS
http://sccode.org/1-5bk
http://sccode.org/1-4Sj chiado de vinil
http://sccode.org/1-5aV ritmo
http://sccode.org/1-5aC guitarra
http://sccode.org/1-58T
http://sccode.org/1-523 bateria
http://sccode.org/1-524 arquivo-x
http://sccode.org/1-57f caixa bateria
http://sccode.org/1-522 teclado massa, melodia
http://sccode.org/1-4Wr slider
http://sccode.org/1-4RF noise
http://sccode.org/1-4Uz voz com slider
*/

 // loop to read Arduino serial from HCSR-04 sensor
  // also, read on/off buttons
s.boot;
s.waitForBoot;

// hello world
{ SinOsc.ar(440) }.play;

(
~distance = Bus.control(s, 1);
~shake = Bus.control(s, 1);
//~button1 = Bus.control(s, 1);
//~button2 = Bus.control(s, 1);
//~button3 = Bus.control(s, 1);
//~button4 = Bus.control(s, 1);
//~button5 = Bus.control(s, 1);
~port = SerialPort("/dev/ttyUSB0", baudrate: 115200); // crtscts: true);
~loop = Routine({ inf.do{
	d = ~port.read; // distance
	k = ~port.read; // shake
//	q = ~port.read; // button 5
//	p = ~port.read; // button 4
//	o = ~port.read; // button 3
//	n = ~port.read; // button 2
///	m = ~port.read; // button 1
	("distance: " + d).postln;
	("shake: " + k).postln;
//	("button1: " + q).postln;
//	("button2: " + p).postln;
//	("button3: " + o).postln;
//	("button4: " + n).postln;
//	("button5: " + m).postln;
	~distance.set(d);
	~shake.set(k);
//	~button5.set(m);
//	~button4.set(n);
//	~button3.set(o);
//	~button2.set(p);
//	~button1.set(q);
}}).play;
)

SerialPort.listDevices();

s.boot;
p = S


erialPort("/dev/ttyUSB0", baudrate: 115200); // crtscts: true);
p.read;

p.readString;