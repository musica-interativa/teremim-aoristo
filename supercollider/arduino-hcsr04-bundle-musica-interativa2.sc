s.boot

"arduino-hcsr04-init.sc".loadRelative

(
// instrumento 01
x = SynthDef("arduino_synth", {
  arg freq = 440, modfreq = 1, amp = 0.3, attack = 0.5, dur = 0.2, pos = 0.5;
  var carrier, modulator, env;
  modulator = LFSaw.kr(modfreq).range(0.5, 0.7);
  carrier = LFSaw.ar(freq: freq, mul: modulator);
  env = Env.perc(attackTime: attack, releaseTime: dur - attack, level: amp).kr(2);
  carrier = carrier * env;
  Out.ar(0, Pan2.ar(carrier, pos))
}).add;

// instrumento 05
y = SynthDef(\supermandolin, {|out, sustain=1, pan, accelerate, freq, detune=0.2 |
	var env = EnvGen.ar(Env.linen(0.002, 0.996, 0.002, 1,-3), timeScale:sustain, doneAction:2);
	var sound = Decay.ar(Impulse.ar(0,0,0.2), 0.1*(freq.cpsmidi)/69) * WhiteNoise.ar;
	var pitch = freq * Line.kr(1, 1+accelerate, sustain);
	sound = CombL.ar(sound, 0.05, pitch.reciprocal*(1-(detune/100)), sustain)
	+ CombL.ar(sound, 0.05, pitch.reciprocal*(1+(detune/100)), sustain);
	OffsetOut.ar(out, DirtPan.ar(sound, 2, pan, env))
}).add;

// instrumento 06
z = SynthDef(\superpwm, {|out, speed=1, decay=0, sustain=1, pan, accelerate, freq,
	voice=0.5, semitone=12, resonance=0.2, lfo=1, pitch1=1|
	var env = EnvGen.ar(Env.pairs([[0,0],[0.05,1],[0.2,1-decay],[0.95,1-decay],[1,0]], -3), timeScale:sustain, doneAction:2);
	var env2 = EnvGen.ar(Env.pairs([[0,0.1],[0.1,1],[0.4,0.5],[0.9,0.2],[1,0.2]], -3), timeScale:sustain/speed);
	var basefreq = freq * Line.kr(1, 1+accelerate, sustain);
	var basefreq2 = basefreq / (2**(semitone/12));
	var lfof1 = min(basefreq*10*pitch1, 22000);
	var lfof2 = min(lfof1 * (lfo + 1), 22000);
	var sound = 0.7 * PulseDPW.ar(basefreq) * DelayC.ar(PulseDPW.ar(basefreq), 0.2, Line.kr(0,voice,sustain)/basefreq);
	sound = 0.3 * PulseDPW.ar(basefreq2) * DelayC.ar(PulseDPW.ar(basefreq2), 0.2, Line.kr(0.1,0.1+voice,sustain)/basefreq) + sound;
	sound = MoogFF.ar(sound, SinOsc.ar(basefreq/32*speed, 0).range(lfof1,lfof2), resonance*4);
	sound = MoogFF.ar(sound, min(env2*lfof2*1.1, 22000), 3);
	sound = sound.tanh*5;
	OffsetOut.ar(out, DirtPan.ar(sound, 2, pan, env));
}).add;

r = Routine({ inf.do {
	if (q == 1, // button 1
		{ // true
			Synth("arduino_synth", [
				\freq, 380 + d,
				\modfreq, (k * 2) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ x.free }  // false
	);
	if (p == 1, // button 2
		{ // true
			Synth.new(\supermandolin, [
				\freq, d.linlin(0, 30, 800, 300),
				\sustain, k.linlin(0, 100, 1, 10),
				\detune, k.linlin(0, 100, 0.2, 1),
				\gain, 1
			])
		},
		{ y.free } // false
	);
	if (o == 1, // button 3
		{ // true
			Synth.new(\superpwm, [
				\freq, d.linlin(0, 30, 400, 100),
				\sustain, k.linlin(0, 100, 0.2, 5),
				\semitone, k.linlin(0, 100, 0, 13),
				\lfo, d.linlin(0, 30, 0, 9),
				\voice, k,
			])
		},
		{ z.free }  // false
	);
	if (n == 1, // button 4
		{ // true
			Synth.new(\superpwm, [
				\freq, d.linlin(0, 30, 400, 100),
				\sustain, k.linlin(0, 100, 0.2, 5),
				\semitone, k.linlin(0, 100, 0, 13),
				\lfo, d.linlin(0, 30, 0, 9),
				\voice, k,
			])
		},
		{ z.free }  // false
	);
	if (m == 1, // button 5
		{ // true
			Synth.new(\supermandolin, [
				\freq, d.linlin(0, 30, 300, 100),
				\sustain, k.linlin(0, 100, 5, 15),
				\detune, k.linlin(0, 100, 0.5, 2),
			])
		},
		{ y.free }  // false
	);
	0.2.wait;
}}).play;
)


// (início) sons para o projeto música interativa, viva bossa, viva palhoça

// y x z w p

(
// instrumento 01
y  = SynthDef("reed_y", {
    arg out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3;
      var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;

x = SynthDef("reed_x", {
    arg out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3;
      var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;

z = SynthDef("reed_z", {
    arg out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3;
      var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;

w = SynthDef("reed_w", {
    arg out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3;
      var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;

p = SynthDef("reed_p", {
    arg out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3;
      var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;
)

(
x.free;
y.free;
z.free;
w.free;
p.release;
)

x.set(\amp, 0.1);

(
r = Routine({ inf.do {
	if (q == 1, // button 1
		{ // true
			Synth("reed_x", [
				\freq, 800 + d,
				\modfreq, (k * 5) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ x.free }  // false
	);
	if (p == 1, // button 2
		{ // true
			Synth("reed_y", [
				\freq, 850 + d,
				\modfreq, (k * 4) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ y.free }  // false
	);
	if (o == 1, // button 3
		{ // true
			Synth("reed_z", [
				\freq, 900 + d,
				\modfreq, (k * 3) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ z.free }  // false
	);
	if (n == 1, // button 4
		{ // true
			Synth("reed_w", [
				\freq, 950 + d,
				\modfreq, (k * 2) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ w.free }  // false
	);
	if (m == 1, // button 5
		{ // true
			Synth("reed_p", [
				\freq, 1000 + d,
				\modfreq, (k * 1) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ p.free }  // false
	);
	0.5.wait;
}}).play;
)


// (fim) sons para projeto música interativa, viva bossa, viva palhoça


// (inicio) experimento de outra musica - ensaio 19 set 2019

(
y = SynthDef(\reed_y, {
    |out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3|
    var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;

x = SynthDef(\reed_x, {
    |out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3|
    var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;

w = SynthDef(\reed_w, {
    |out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3|
    var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;

p = SynthDef(\reed_p, {
    |out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3|
    var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;

z = SynthDef(\reed_z, {
    |out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3|
    var snd, blow;
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;
)

// abaixo codigo retirado do exemplo sem alteracao
Pbind(
    \instrument, \reed,
    \amp, 0.1*(2**Pgauss(0, 0.1)),
    \dur, 5.0,
    \legato, 1,
    \root, 1,
    \attack, 0.2,
    \release, 0.2,
    \degree, Pseq([[-7, -3, 0, 2], [-7, -2, 0, 3], [-7, -1, 1, 4]].mirror1, inf)
).play;

// y x z w p

// nao esta desligando...
(
r = Routine({ inf.do {
	if (q == 1, // button 1
		{ // true
			Synth("reed_x", [
				\freq, 800 + d,
				\modfreq, (k * 5) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ x.free }  // false
	);
	if (p == 1, // button 2
		{ // true
			Synth("reed_y", [
				\freq, 850 + d,
				\modfreq, (k * 4) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ y.free }  // false
	);
	if (o == 1, // button 3
		{ // true
			Synth("reed_z", [
				\freq, 900 + d,
				\modfreq, (k * 3) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ z.free }  // false
	);
	if (n == 1, // button 4
		{ // true
			Synth("reed_w", [
				\freq, 950 + d,
				\modfreq, (k * 2) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ w.free }  // false
	);
	if (m == 1, // button 5
		{ // true
			Synth("reed_p", [
				\freq, 1000 + d,
				\modfreq, (k * 1) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ p.free }  // false
	);
	0.5.wait;
}}).play;
)

// (fim) experimento de outra musica - ensaio 19 set 2019

(
x.free;
y.free;
z.free;
r.stop;
w.stop;
p.stop;
)

"arduino-hcsr04-stop.sc".loadRelative