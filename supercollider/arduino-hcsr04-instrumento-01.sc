"arduino-hcsr04-init.sc".loadRelative

( // instrumento 01: SynthDef arduino_synth
SynthDef("arduino_synth", {
  arg freq = 440, modfreq = 1, amp = 0.3, attack = 0.5, dur = 0.2, pos = 0.5;
  var carrier, modulator, env;
  modulator = LFSaw.kr(modfreq).range(0.5, 0.7);
  carrier = LFSaw.ar(freq: freq, mul: modulator);
  env = Env.perc(attackTime: attack, releaseTime: dur - attack, level: amp).kr(2);
  carrier = carrier * env;
  Out.ar(0, Pan2.ar(carrier, pos))
}).add;
)

(
r = Routine({ inf.do {
	Synth("arduino_synth", [
		\freq, 380 + d,
		\modfreq, (k * 2) / 100,
		\amp, k.linlin(0, 100, 0.1, 1),
	]);
	0.2.wait;
}}).play;
)

r.stop;

"arduino-hcsr04-stop.sc".loadRelative