// teremin aoristo NANO

 // loop to read Arduino serial from HCSR-04 sensor
  // also, read on/off buttons

(
s.boot;
s.waitForBoot;
~distance = Bus.control(s, 1);
~shake = Bus.control(s, 1);
~port = SerialPort("/dev/ttyUSB0", baudrate: 115200); // crtscts: true);
~loop = Routine({ inf.do{
	d = ~port.read; // distance
	k = ~port.read; // shake
	("distance: " + d).postln;
	("shake: " + k).postln;
	~distance.set(d);
	~shake.set(k);
}}).play;
)

// listar portas serial (DEBUG)
SerialPort.listDevices();

// INSTRUMENTO 02

( // instrumento 02: BMoog
SynthDef(\bmoog, {
	var sig, freq;
	freq = (In.kr(~distance.index) * 10);
    sig = BMoog.ar(
        LFSaw.ar([freq * 0.99, freq * 1.01], 0, 0.1) ! 2,
        SinOsc.kr(SinOsc.kr(0.1),1.5pi,1550,1800),
		In.kr(~shake.index) / 100,
        LFSaw.kr(1,0,3));    // filter mode - sweep modes
	Out.ar(0, (CombN.ar(sig, 0.4, [0.4,0.35],2) * 0.4) + (sig * 0.5))
}).add;
);
~bmoog = Synth(\bmoog);

// INSTRUMENTO 01

( // instrumento 01: SynthDef arduino_synth
SynthDef("arduino_synth", {
  arg freq = 440, modfreq = 1, amp = 0.3, attack = 0.5, dur = 0.2, pos = 0.5;
  var carrier, modulator, env;
  modulator = LFSaw.kr(In.kr((~shake.index *2) / 100)).range(0.5, 0.7);
	carrier = LFSaw.ar(freq: (380 + In.kr(~distance.index)), mul: modulator);
	env = Env.perc(attackTime: attack, releaseTime: dur - attack, level: (In.kr(~shake.index).linlin(0, 100, 0.1, 1))).kr(2);
  carrier = carrier * env;
  Out.ar(0, Pan2.ar(carrier, pos))
}).add;
);

~arduino = Synth("arduino_synth");

// INSTRUMENTO: reed

(
SynthDef(\reed, {
    |out = 0, freq = 440, amp = 0.1, gate = 1, attack = 0.3, release = 0.3|
	var snd, blow;
	freq = 100 + (In.kr(~distance.index) * 3);
	amp = In.kr(~shake.index).linlin(0, 100, 0.1, 1);
    // pulse with modulating width
    snd = Pulse.ar((Rand(-0.03, 0.05) + freq.cpsmidi).midicps, 0.48 + LFNoise1.kr(0.06, 0.1), 0.2);
    // add a little "grit" to the reed
    snd = Disintegrator.ar(snd, 0.5, 0.7);
    // a little ebb and flow in volume
    snd = snd * LFNoise2.kr(5, 0.05, 1);
    // use the same signal to control both the resonant freq and the amplitude
    blow = EnvGen.ar(Env.asr(attack, 1.0, release), gate, doneAction: 2);
    snd = snd + BPF.ar(snd, blow.linexp(0, 1, 2000, 2442), 0.3, 3);
    // boost the high end a bit to get a buzzier sound
    snd = BHiShelf.ar(snd, 1200, 1, 3);
    snd = snd * blow;
    Out.ar(out, Pan2.ar(snd, 0, amp));
}).add;
)

~reed = Synth(\reed);

// instrumento: 05

(
SynthDef(\supermandolin, { arg out, pan, accelerate;
	var freq = In.kr(~distance.index).linlin(0, 30, 800, 300);
	var sustain = In.kr(~shake).linlin(0, 100, 1, 10);
	var detune = In.kr(~shake).linlin(0, 100, 0.2, 1);
	var env = EnvGen.ar(Env.linen(0.002, 0.996, 0.002, 1,-3), timeScale:sustain, doneAction:2);
	var sound = Decay.ar(Impulse.ar(0,0,0.2), 0.1*(freq.cpsmidi)/69) * WhiteNoise.ar;
	var pitch = freq * Line.kr(1, 1+accelerate, sustain);
	sound = CombL.ar(sound, 0.05, pitch.reciprocal*(1-(detune/100)), sustain)
	+ CombL.ar(sound, 0.05, pitch.reciprocal*(1+(detune/100)), sustain);
	Out.ar(out, DirtPan.ar(sound, 2, pan, env))
}).add;
)

Synth.new(\supermandolin)

// instrumento 06
z = SynthDef(\superpwm, {|out, speed=1, decay=0, sustain=1, pan, accelerate, freq,
	voice=0.5, semitone=12, resonance=0.2, lfo=1, pitch1=1|
	var env = EnvGen.ar(Env.pairs([[0,0],[0.05,1],[0.2,1-decay],[0.95,1-decay],[1,0]], -3), timeScale:sustain, doneAction:2);
	var env2 = EnvGen.ar(Env.pairs([[0,0.1],[0.1,1],[0.4,0.5],[0.9,0.2],[1,0.2]], -3), timeScale:sustain/speed);
	var basefreq = freq * Line.kr(1, 1+accelerate, sustain);
	var basefreq2 = basefreq / (2**(semitone/12));
	var lfof1 = min(basefreq*10*pitch1, 22000);
	var lfof2 = min(lfof1 * (lfo + 1), 22000);
	var sound = 0.7 * PulseDPW.ar(basefreq) * DelayC.ar(PulseDPW.ar(basefreq), 0.2, Line.kr(0,voice,sustain)/basefreq);
	sound = 0.3 * PulseDPW.ar(basefreq2) * DelayC.ar(PulseDPW.ar(basefreq2), 0.2, Line.kr(0.1,0.1+voice,sustain)/basefreq) + sound;
	sound = MoogFF.ar(sound, SinOsc.ar(basefreq/32*speed, 0).range(lfof1,lfof2), resonance*4);
	sound = MoogFF.ar(sound, min(env2*lfof2*1.1, 22000), 3);
	sound = sound.tanh*5;
	OffsetOut.ar(out, DirtPan.ar(sound, 2, pan, env));
}).add;


// instrumento novo

(
SynthDef(\novo, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = SinOsc.ar((100 - (distance * 2)) * 2);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\novo);

// instrumento novo: SAW

(
SynthDef(\sawosc, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = Saw.ar((100 - (distance * 2)) * 2);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\sawosc);

// instrumento novo: SAW

(
SynthDef(\triosc, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = LFTri.ar((100 - (distance * 2)) * 2);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\triosc);

// instrumento novo: SAW

(
SynthDef(\parosc, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = LFPar.ar((100 - (distance * 2)) * 2);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\parosc);

(
SynthDef(\varsaw, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = VarSaw.ar((100 - (distance * 2)) * 2);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\varsaw);

(
SynthDef(\varsaw, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = VarSaw.ar((100 - (distance * 2)) * 2) + SinOsc.ar(Vibrato.kr(shake) * 2);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\varsaw);

// INSTRUMENTO: breakcore

Breakcore.ar()



(
SynthDef(\arneodo, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = ArneodoCoulletTresser.ar(distance.linlin(0, 35, 22000, 36000));
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\arneodo);

{ CuspN.ar(250) }.play


(
{
    var f, sf;
    f = Ball.ar(MouseY.kr(0.01, 20, 1));
    f = f * 10 + 500;
    SinOsc.ar(f, 0, 0.2)
}.play;
)

(
{ var excitation = EnvGen.kr(Env.perc,
                            MouseButton.kr(0, 1, 0),
                             timeScale: 0.1, doneAction: 0
                            ) * PinkNoise.ar(0.4);
  var tension = MouseX.kr(0.01, 0.1);
  var loss = MouseY.kr(0.999999, 0.999, 1);
  MembraneCircle.ar(excitation, tension, loss);
}.play;
)


// several springs in series.
// trigger gate is mouse button
// spring constant is mouse x
// mouse y controls damping
(
{     var m0, m1, m2, m3, d, k, inforce;
    d = MouseY.kr(0.00001, 0.01, 1);
    k = MouseX.kr(0.1, 20, 1);
    inforce = K2A.ar(MouseButton.kr(0,1,0)) > 0;
    m0 = Spring.ar(inforce, k, 0.01);
    m1 = Spring.ar(m0, 0.5 * k, d);
    m2 = Spring.ar(m0, 0.6 * k + 0.2, d);
    m3 = Spring.ar(m1 - m2, 0.4, d);
    SinOsc.ar(m3 * 200 + 500, 0, 0.2) // modulate frequency with the force

}.play;
)



// several springs in series.
// trigger gate is mouse button
// spring constant is mouse x
// mouse y controls damping
(
Synth
{


}.play;



)


(
SynthDef(\spring, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound; // = LFTri.ar((100 - (distance * 2)) * 2);
	var m0, m1, m2, m3, d, k, inforce;
    d = distance; //.kr(0.00001, 0.01, 1);
    k = shake; //.kr(0.1, 20, 1);
    inforce = K2A.ar(d) > 0;
    m0 = Spring.ar(inforce, k, 0.01);
    m1 = Spring.ar(m0, 0.5 * k, d);
    m2 = Spring.ar(m0, 0.6 * k + 0.2, d);
    m3 = Spring.ar(m1 - m2, 0.4, d);
    sound = SinOsc.ar(m3 * 200 + 500, 0, 0.2); // modulate frequency with the force
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\spring)

// AY *****************************************************
// Emulator of the AY (aka YM) soundchip, used in Spectrum/Atari

x = {AY.ar * 0.1}.play(s);
x.free;


// Mouse-controlled tones
(
x = {
    Pan2.ar(AY.ar(
        tonea: In.kr(~distance.index) * 20,
        toneb:     In.kr(~shake.index),
        control:     3,
        vola:     14,
        volb:     14,
        volc:     0,
        mul: 0.1
    ))
}.play;
)

// PulseDPW

{ Pulse   .ar(XLine.kr(40,4000,6),0.1, 0.2) }.play;
{ PulseDPW.ar(XLine.kr(40,4000,6),0.1, 0.2) }.play;


(
SynthDef(\vosim, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 1);
	var sound = VOSIM.ar(Impulse.ar(100), distance * (10 + shake), 3, 0.99);
	Out.ar(0, Pan2.ar(sound, 0, amp));
}).add;
)

Synth(\vosim);


z = {}.play;
