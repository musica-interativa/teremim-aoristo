// SANTA TECNOLOGIA
// notas:
// G, F, C,
// Eb, G

(
s.waitForBoot {
"_teremim-aoristo-serial-gui-log.scd".loadRelative;
~dict.add(1 -> 220).postln;
~dict.add(2 -> 220).postln;
~dict.add(3 -> 220).postln;
~dict.add(4 -> 220).postln;
~dict.add(5 -> 220).postln;
~dict.add(6 -> 220).postln;
~dict.add(7 -> 220).postln;
~dict.add(8 -> 220).postln;
~dict.add(9 -> 220).postln;
~dict.add(10 -> 440).postln;
~dict.add(11 -> 440).postln;
~dict.add(12 -> 440).postln;
~dict.add(13 -> 440).postln;
~dict.add(14 -> 440).postln;
~dict.add(15 -> 440).postln;
~dict.add(16 -> 440).postln;
~dict.add(17 -> 440).postln;
~dict.add(18 -> 440).postln;
~dict.add(19 -> 440).postln;
~dict.add(20 -> 880).postln;
~dict.add(21 -> 880).postln;
~dict.add(22 -> 880).postln;
~dict.add(23 -> 880).postln;
~dict.add(24 -> 880).postln;
~dict.add(25 -> 880).postln;
~dict.add(26 -> 880).postln;
~dict.add(27 -> 880).postln;
~dict.add(28 -> 880).postln;
~dict.add(29 -> 880).postln;
~dict.add(30 -> 220).postln;
~dict.add(31 -> 220).postln;
~dict.add(32 -> 220).postln;
~dict.add(33 -> 220).postln;
~dict.add(34 -> 220).postln;
~dict.add(35 -> 220).postln;
})

// INSTRUMENTO: chaos
(
SynthDef(\chaos, {
	var distance = In.kr(~distance.index);
	var shake = In.kr(~shake.index);
	var amp = shake.linlin(0, 100, 0.1, 0.5);
	var sound = SinOsc.ar(distance + (shake * SinOsc.kr([50, 51], 0, SinOsc.kr(101, Saw.kr(0.12345, 678, 9), 0.2, 0.8), Pulse.kr([25, 25.5], 0.25, 0.125, -0.25))), 0, 0.5, 0);
	Out.ar(0, Pan2.ar(Vibrato.ar(sound + SinOsc.ar(200 - (distance * 5)), shake, shake / 100), 0, amp));
}).add;
);

Synth(\chaos)