#include <Thread.h>

/*
   The Teremim Aoristo project (NANO version)

   author: Joenio Costa <joenio AT joenio DOT me>
   year: 2019
   license: GPLv3

   This project is part of the Musica Interativa project funded by Brazil's
   District Federal FAC and all source code and documents is available at:
   https://gitlab.com/musica-interativa/teremim-aoristo

*/

/*
   DEBUG mode enables through Serial:
     + The value read from the HC-SR04 distance sensor
*/
const boolean DEBUG = false; // remember to disable DEBUG before run supercollider code

// remember to edit the PINs where HCSR-04 is connected
struct HCSR04 {
  const unsigned char TRIG = 13;
  const unsigned char ECHO = 12;
  long duration = 0;
  int distance = 0;
  int delta = 0;
  const unsigned char MAX_DISTANCE = 35; // (in centimeters) values out of max distance are ignored
  int previous_distance = 0;
  signed char shake = 50; // values: [0 ~ 100], default value: any arbitrary initial value greather than 0
  const unsigned char MAX_SHAKE = 100;
  unsigned long idle_time = 0;
} hc;

/*
   clear_screen()

   clear serial terminal using ANSI escape sequences when in DEBUG mode.
   thanks arduino forum to share this code:
   https://forum.arduino.cc/index.php?topic=107488.0
*/
void clear_screen() {
  Serial.write(27); // ESC
  Serial.print("[2J"); // clear screen
  Serial.write(27); // ESC
  Serial.print("[H"); // cursor to home
}

Thread read_distance = Thread();
Thread increase_shake = Thread();
Thread decrease_shake = Thread();
Thread write_to_serial = Thread();
Thread set_idle_time = Thread();

/*
   increase_shake_callback()

   calculate shake based on diferences detected on
   distance read from HC-SR04 sensor and IDLE time
*/
void increase_shake_callback() {
  signed char shake = hc.shake;
  if (hc.idle_time < 1000 && hc.delta > 2) { // increase shake only when not IDLE
    shake = hc.shake + 3; // increase rate +3
  }
  else if (hc.idle_time > 2000 && shake == 0) {
    shake = hc.shake + 10;
  }
  // ensure shake is between threshold values
  if (shake > hc.MAX_SHAKE) {
    shake = hc.MAX_SHAKE;
  }
  hc.shake = shake;
}

/*
   decrease_shake_callback()

   calculate shake based on diferences detected on
   distance read from HC-SR04 sensor and IDLE time
*/
void decrease_shake_callback() {
  signed char shake = hc.shake;
  if (hc.idle_time > 1000) { // IDLE greather than 1 second
    // no movement detected
    shake = hc.shake - 2;
  }
  // ensure shake is between threshold values
  if (shake < 0) {
    shake = 0;
  }
  hc.shake = shake;
}

/*
   read_distance_callback()

   read HC-SR04 sensor
*/
void read_distance_callback() {
  // Clears the trigPin
  digitalWrite(hc.TRIG, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(hc.TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(hc.TRIG, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  hc.duration = pulseIn(hc.ECHO, HIGH);
  // Calculating the distance
  hc.distance = hc.duration * 0.034 / 2;
  hc.delta = abs(hc.distance - hc.previous_distance);
  if (hc.distance > hc.MAX_DISTANCE || hc.delta > hc.MAX_DISTANCE) {
    hc.distance = hc.previous_distance;
  }
}

/*
  set_idle_time_callback()

  calculate IDLE time based on presense detected by sensor
*/
void set_idle_time_callback() {
  if (abs(hc.distance - hc.previous_distance)) { // presence detected
    hc.idle_time = 0;
  }
  else { // no presence
    hc.idle_time = hc.idle_time + 100; // use the same value defined for thread interval on setup()
  }
}

/*
   write_to_serial_callback()

   send the values read from buttons, sensors, and others controlles over serial
*/
void write_to_serial_callback() {
  if (read_distance.enabled) { // check if hc-sr04 sensor enabled
    unsigned char buf[2];
    buf[0] = (unsigned char) hc.distance;
    buf[1] = (unsigned char) hc.shake;
    if (DEBUG) {
      clear_screen();
      Serial.print("distance: ");
      Serial.println(hc.distance);
      Serial.print("shake: ");
      Serial.println(hc.shake);
      Serial.print("idle_time(ms): "); // 60.000 ms = 1 min
      Serial.println(hc.idle_time);
      Serial.print("MAX_SHAKE: ");
      Serial.println(hc.MAX_SHAKE);
      Serial.print("MAX_DISTANCE: ");
      Serial.println(hc.MAX_DISTANCE);
    }
    else {
      Serial.write(buf, 2); // send 2 bytes: distance, shake
    }
    hc.previous_distance = hc.distance;
  }
}

void setup() {
  // init HC-SR04 sensor
  pinMode(hc.TRIG, OUTPUT);
  pinMode(hc.ECHO, INPUT);

  Serial.begin(115200); // remember to use the same rate on supercollider code

  read_distance.enabled = true;
  read_distance.setInterval(25); // 25ms
  read_distance.onRun(read_distance_callback);

  increase_shake.enabled = true;
  increase_shake.setInterval(250); // 250ms
  increase_shake.onRun(increase_shake_callback);

  decrease_shake.enabled = true;
  decrease_shake.setInterval(250); // 250ms
  decrease_shake.onRun(decrease_shake_callback);

  set_idle_time.enabled = true;
  set_idle_time.setInterval(100); // 100ms (make sure this value is the same used in set_idle_time() function)
  set_idle_time.onRun(set_idle_time_callback);

  write_to_serial.enabled = true;
  write_to_serial.setInterval(50); // 50ms
  write_to_serial.onRun(write_to_serial_callback);
}

void loop() {
  if (read_distance.shouldRun())
    read_distance.run();
  if (decrease_shake.shouldRun())
    decrease_shake.run();
  if (increase_shake.shouldRun())
    increase_shake.run();
  if (set_idle_time.shouldRun())
    set_idle_time.run();
  if (write_to_serial.shouldRun())
    write_to_serial.run();
}
