#include <Thread.h>

/*
   The Teremim Aoristo project

   author: Joenio Costa <joenio AT joenio DOT me>
   year: 2019
   license: GPLv3

   This project is part of the Musica Interativa project funded by Brazil's
   District Federal FAC and all source code and documents is available at:
   https://gitlab.com/musica-interativa/teremim-aoristo

*/

/*
   DEBUG mode enables through Serial:
     + Messages about leds and buttons status
     + The value read from the HC-SR04 distance sensor
     + Blinking leds for about 10 seconds on boot
*/
const boolean DEBUG = false; // remember to disable DEBUG before run supercollider code

// remember to edit the PINs where HC-SR04 is connected
struct HCSR_04 {
  const unsigned char TRIG = 13;
  const unsigned char ECHO = 12;
  long duration = 0;
  int distance = 0;
  const unsigned char MAX_DISTANCE = 35; // (in centimeters) values out of max distance are ignored
  int previous_distance = 0;
  int delta = 0;
  signed char shake = 50; // values: [0 ~ 100], default value: any arbitrary initial value greather than 0
  const boolean SEND_CONTINUALLY = true; // if false send only distinct values
  const unsigned char MAX_SHAKE = 100;
} hc;

// edit to add/remove buttons and to map
// each PIN button and PIN led
const unsigned char NUMBER_OF_BUTTONS = 5;
struct BUTTONS {
  const unsigned char buttonsPin[NUMBER_OF_BUTTONS] = {6, 5, 4, 3, 2}; // buttons on <N> pins
  const unsigned char ledsPin[NUMBER_OF_BUTTONS] = {A4, A3, A2, A1, A0}; // leds on A<N> pins
  unsigned char buttonsState[NUMBER_OF_BUTTONS] = {0};
  int flags[NUMBER_OF_BUTTONS] = { -1};
  boolean flag[NUMBER_OF_BUTTONS] = {false};
  unsigned char count[NUMBER_OF_BUTTONS] = {0}; // to count how many main loop button keep pressed
  boolean ledsFlag[NUMBER_OF_BUTTONS] = {false};
} btn;

/*
   clear_screen()

   clear serial terminal using ANSI escape sequences.
   thanks arduino forum to share this code, copied from:
   https://forum.arduino.cc/index.php?topic=107488.0
*/
void clear_screen() {
  Serial.write(27); // ESC
  Serial.print("[2J"); // clear screen
  Serial.write(27); // ESC
  Serial.print("[H"); // cursor to home
}

void led_on(int pin) {
  digitalWrite(pin, HIGH); // led on
}

void led_off(int pin) {
  digitalWrite(pin, LOW); // led off
}

// this is just for DEBUG the leds
void all_leds_on() {
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++)
    led_on(btn.ledsPin[i]);
}

// this is just for DEBUG the leds
void all_leds_off() {
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++)
    led_off(btn.ledsPin[i]);
}

/*
   test_all_leds()

   just debugs leds and its circuit, turning on and off all leds
   three times with 500ms interval, then turn on and off each led
   individually with the same 500ms interval
*/
void test_all_leds() {
  const unsigned char interval_between_blink = 200;
  int count = 3;
  while (count > 0) {
    all_leds_on();
    delay(interval_between_blink);
    all_leds_off();
    delay(interval_between_blink);
    count = count - 1;
  }
  count = 3;
  while (count > 0) {
    for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
      led_on(btn.ledsPin[i]);
      delay(interval_between_blink);
      count = count - 1;
      led_off(btn.ledsPin[i]);
    }
    count = count - 1;
  }
}

Thread read_buttons = Thread();
Thread read_distance = Thread();
Thread write_to_serial = Thread();

/*
   read_buttons_callback()

   read button state (pressed or not pressed?)
*/
void read_buttons_callback() {
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    btn.buttonsState[i] = digitalRead(btn.buttonsPin[i]);
    if (btn.buttonsState[i] == HIGH) { // botao pressionado
      btn.count[i] = btn.count[i] + 1;
    }
    else if (btn.buttonsState[i] == LOW) { // botao solto
      if (btn.count[i] > 3) { // doing a very basic bounce/debounce here
        btn.flag[i] = not btn.flag[i];
        btn.count[i] = 0;
      }
    }
    if (btn.buttonsState[i] == LOW) { // botao solto
      // turn led on!
      if (btn.flag[i]) {
        digitalWrite(btn.ledsPin[i], HIGH); // led on
        btn.ledsFlag[i] = true;
      }
      // turn led off!
      else {
        digitalWrite(btn.ledsPin[i], LOW); // led off
        btn.ledsFlag[i] = false;
      }
    }
  }
}

/*
   read_distance_callback()

   read HCSR 04 sensor
*/
void read_distance_callback() {
  // Clears the trigPin
  digitalWrite(hc.TRIG, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(hc.TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(hc.TRIG, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  hc.duration = pulseIn(hc.ECHO, HIGH);
  // Calculating the distance
  hc.distance = hc.duration * 0.034 / 2;
  int delta = abs(hc.distance - hc.previous_distance);
  if (hc.distance > hc.MAX_DISTANCE || delta > hc.MAX_DISTANCE) {
    hc.distance = hc.previous_distance;
  }
  hc.delta = abs(hc.distance - hc.previous_distance);
  if (hc.delta == 0) {
    hc.shake = hc.shake - 1;
  }
  else if (hc.delta <= 2) {
    hc.shake = hc.shake; // this line is just for better readabilitty
  }
  else {
    hc.shake = hc.shake + 3; // the increase rate is greather than decrease rate
  }
  if (hc.shake < 0) {
    hc.shake = 0;
  }
  else if (hc.shake > hc.MAX_SHAKE) {
    hc.shake = hc.MAX_SHAKE;
  }
}

/*
   write_to_serial_callback()

   send the values read from buttons, sensors, and others controlles over serial
*/
void write_to_serial_callback() {
  if (DEBUG)
    clear_screen();
  if (read_distance.enabled) { // check if hc-sr04 sensor enabled
    unsigned char buf[2];
    buf[0] = (unsigned char) hc.distance;
    buf[1] = (unsigned char) hc.shake;
    if (DEBUG) {
      Serial.print("distance: ");
      Serial.println(hc.distance);
      Serial.print("shake: ");
      Serial.println(hc.shake);
    }
    else {
      Serial.write(buf, 2); // send 2 bytes: distance, shake
    }
    hc.previous_distance = hc.distance;
  }
  if (read_buttons.enabled) { // read_buttons thread enabled?
    unsigned char buf_btn[NUMBER_OF_BUTTONS];
    for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
      buf_btn[i] = btn.flag[i];
    }
    if (DEBUG) {
      for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
        Serial.print("button[");
        Serial.print(i);
        Serial.print(", pin=");
        Serial.print(btn.buttonsPin[i]);
        Serial.print("]: ");
        Serial.print(btn.flag[i]);
        Serial.print(", led[");
        Serial.print(i);
        Serial.print(", pin=");
        Serial.print(btn.ledsPin[i]);
        Serial.print("]: ");
        Serial.println(btn.ledsFlag[i]);
      }
    }
    else { // send buttons to serial
      Serial.write(buf_btn, NUMBER_OF_BUTTONS); // send NUMBER_OF_BUTTONS bytes
    }
  }
  if (DEBUG) {
    Serial.print("MAX_SHAKE: ");
    Serial.println(hc.MAX_SHAKE);
    Serial.print("MAX_DISTANCE: ");
    Serial.println(hc.MAX_DISTANCE);
  }
}

void setup() {
  // init HC-SR04 sensor
  pinMode(hc.TRIG, OUTPUT);
  pinMode(hc.ECHO, INPUT);
  // init buttons and leds
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    pinMode(btn.ledsPin[i], OUTPUT);
    pinMode(btn.buttonsPin[i], INPUT_PULLUP);
  }
  if (DEBUG)
    test_all_leds();
  Serial.begin(115200); // remember to use the same rate on supercollider code

  read_buttons.enabled = true;
  read_buttons.setInterval(25); // 25ms
  read_buttons.onRun(read_buttons_callback);

  read_distance.enabled = true;
  read_distance.setInterval(25); // 25ms
  read_distance.onRun(read_distance_callback);

  write_to_serial.enabled = true;
  write_to_serial.setInterval(50); // 50ms
  write_to_serial.onRun(write_to_serial_callback);
}

void loop() {
  if (read_buttons.shouldRun())
    read_buttons.run();
  if (read_distance.shouldRun())
    read_distance.run();
  if (write_to_serial.shouldRun())
    write_to_serial.run();
}
