# Teremim Aoristo

![teremim aoristo logotipo](aoristo-logo-600px.png)

_O nome Aoristo é uma referência à tecnologia "Aorist Rods" do Guia do
Mochileiro das Galáxias_ [1][aorist1] [2][aorist2].

Este projeto de instrumento musical construído a base de Arduino e sensor de
distância ultrasônico HC-SR04 envia os sinais captados da distância da mão do
usuário para um software escrito em SuperCollider, um ambiente de programação
musical cliente-servidor.

A distância entre o sensor HC-SR04 e a mão de quem toca o instrumento é
utilizado como parâmetros de entrada para os algoritmos responsáveis por
sintetizar sons, programado com SuperCollider, além do sensor de distância o
instrumento **Teremim Aoristo** conta também com 5 botões para seleção de
sintetizadores distindos, podendo ser combinados entre sí em qualquer ordem.

Cada sintetizador é implementado em linguagem de programaçao SuperCollider e
recebem parâmetros indicando a distância da mão do usuário ao sensor
ultrasônico e também o movimento feito pela mão, quanto mais movimentos com a
mão maior o segundo argumento chamado de balanço ou movimento (shake, em
inglês) passado ao sintetizador, o valor `shake` varia entre 0 e 100.

O código-fonte utilizado no Arduino pode ser consultado no link abaixo:

* https://gitlab.com/musica-interativa/teremim-aoristo/sketchbook/

Os sintetizadores e códigos responsáveis por processar as entradas em
SuperCollider estão em:

* https://gitlab.com/musica-interativa/teremim-aoristo/supercollider/

A distância máxima captada pelo sensor HC-SR04 é de **35 centímetros**, os
testes realizados durante o desenvolvimento do Teremim Aoristo com valores
acima deste limite apresentaram um nível de ruído do valor captado muito alto,
dessa forma, 30 centímetros parece ser um limite razoável para uma boa
experiência do usuário.

Os dados são enviados por comunicação serial em bytes na velocidade de 115200
bauds (data rate in bits per second).

## Circuito do sensor HC-SR04

![foto botoes, leds e sensor](midia/foto-botoes-led-sensor.jpg)

O circuito foi montado seguindo os links abaixo:

* http://buildbot.com.br/blog/como-utilizar-o-sensor-ultrasonico-hc-sr04/
* https://www.filipeflop.com/produto/sensor-de-distancia-ultrassonico-hc-sr04/
* https://www.filipeflop.com/blog/sensor-ultrassonico-hc-sr04-ao-arduino/

![esquema circuito hc-sr04](Arduino_HC_SR04_bb.png)

_( esquema do circuito HC-SR04 da imagem acima retirada de https://www.filipeflop.com/blog/sensor-ultrassonico-hc-sr04-ao-arduino/ )_

## Circuito botões e leds

![foto botoes e leds](midia/foto-botoes-led.jpg)

Descrição do circuito e conexões:

* Pino `Trig` do HC-SR04 conectado no PIN `13` do Arduino.
* Pino `Echo` do HC-SR04 conectado no PIN `12` do Arduino.
* Pino `Vcc` do HC-SR04 conectado no PIN `5V` do Arduino.
* Pino `Gnd` do HC-SR04 conectado no PIN `GND` do Arduino.

Alguns PINs analógicos do Arduino estão sendo utilizados como digitais.

## Vídeo de demonstração da versão alpha

[![Testing alpha version of the instrument Aorist Theremin](https://img.youtube.com/vi/urR-StOA-LU/0.jpg)](https://youtu.be/urR-StOA-LU)

## Vídeo de demonstração da versão beta

[![Testing beta version of the instrument Aorist Theremin](https://img.youtube.com/vi/z4bjRHLqFQ4/0.jpg)](https://youtu.be/z4bjRHLqFQ4)

## Primeira versão do circuito na caixa

![foto1 protoboard na caixa](midia/foto1-protoboard-na-caixa.jpg)

![foto2 protoboard na caixa](midia/foto2-protoboard-na-caixa.jpg)

![foto3 protoboard na caixa](midia/foto3-protoboard-na-caixa.jpg)

![foto com caixa fechada](midia/foto-caixa-fechada.jpg)

Os furos na caixa foram feitos com uma mini retífica, os botões utilizados para
encaixar nos furos foram reutilizados de um controle remoto de televisão. Foi
utilizado também uma placa de acrílico para posicionar e fixar os circuitos
dentro da caixa.

## Vídeos da primeira versão do circuito na caixa

[![video1 da primeira versao dentro da caixa](midia/video1-primeira-versao-na-caixa.png)](midia/video1-primeira-versao-na-caixa.webm)

[![video2 da primeira versao dentro da caixa](midia/video2-primeira-versao-na-caixa.png)](midia/video2-primeira-versao-na-caixa.webm)

[aorist1]: https://hitchhikers.fandom.com/wiki/Aorist_Rods
[aorist2]: https://sites.google.com/site/h2g2theguide/Index/a/767111

## Ensaio aberto no Zepelim

![foto do teremim no ensaio aberto no Zepelim](midia/ensaio-aberto-zepelim.jpg)

## Fotos do teremim no show do Canteiro Central

Show com a banca Maria Sabina & a Pêia no dia 22 de Novembro de 2019.

![foto do teremim no show do Canteiro Central](midia/encerramento-canteiro-central-1.jpg)

![foto do teremim no show do Canteiro Central](midia/encerramento-canteiro-central-2.jpg)

## Desenvovimento

* Arduino-IDE 1.0.5
* Arduino-Makefile 1.5.2
* Debian testing
* Placa Arduino Uno

Instalar dependencias de compilação:

```console
sudo apt install arduino-mk arduino-core
```

Entre na pasta do código Arduino:

```console
cd sketchbook/aorist_theremin_hcsr04/
```

Para conectar ao monitor serial via linha de comando use o comando abaixo, o
monitor serial será aberto dentro de uma sessão `screen`.

```console
make monitor
```

Para sair do `screen` pressione CTRL+A e K.

Para gravar o código no arduino execute:

```console
make upload
```

O Makefile `Arduino-mk` suporta outras placas além do Arduino Uno, para
checar a lista de placas suportadas execute:

```console
make show_boards
```

## Theremim versão Nano

Versão mínima do Teremim Aoristo sem botões e leds, usando Arduino Nano ao invés
do Arduino Uno, feito apenas com sensor de distância HC-SR04.

![foto prototipo versao nano](midia/prototipo-versao-nano.jpg)

### Desenvolvimento versão Nano

O código-fonte Arduino e SuperCollider estão na sub-pasta `nano/` deste
repositório.

```console
cd nano/sketchbook/
make upload
make monitor
```

# Case para Arduino + sensor HC-SR04

Opções de cases para impressão 3D:

* [Box Arduino Uno and Sensor ultrasonic HC-SR04](https://www.thingiverse.com/thing:3366898)
* [Lolin32, Ultrasonic Sensor, & Battery Case](https://www.thingiverse.com/thing:2961817)
* [My HC-SR04 sensor box](https://www.thingiverse.com/thing:3835325)
* [Housing for Nocemcu and HC-SR04 ultrasonic distance sensor](https://www.thingiverse.com/thing:2550726)
* [Housing for Wemos mini and HC-SR04 ultrasonic distance sensor](https://www.thingiverse.com/thing:3776067)
* [HC-SR04 Control Box](https://www.thingiverse.com/thing:3290458)

Dentre as opções acima as melhores escolhas para o Teremim Aoristo são: (a)
_Lolin32, Ultrasonic Sensor, & Battery Case_ e (b) _HC-SR04 Control Box_, a
opção (a) será utilizada como base e adaptada para as eventuais necessidades do
projeto.

# Problemas e soluções

## Arduino Nano

### Erro `stk500_recv(): programmer is not responding` no upload

Ao fazer upload do código para o Arduino Nano a mensagem abaixo é
exibida indefinidamente até falhar totalmente.

```
avrdude: stk500_recv(): programmer is not responding
avrdude: stk500_getsync() attempt 8 of 10: not in sync: resp=0x00
avrdude: stk500_recv(): programmer is not responding
avrdude: stk500_getsync() attempt 9 of 10: not in sync: resp=0x00
avrdude: stk500_recv(): programmer is not responding
avrdude: stk500_getsync() attempt 10 of 10: not in sync: resp=0x00
Problema ao carregar para a placa. Veja
  http://www.arduino.cc/en/Guide/Troubleshooting#upload para sugestões.
```

### Solução

Alterar o processador para `ATmega328P (Old Bootloader)` no Arduino IDE resolve
o problema.

## Referencias teóricas

Artigos com temas teóricos para referenciar em descrição sobre o teremim:

* https://www.academia.edu/14648151/A_Study_with_Hyperinstruments_in_Free_Musical_Improvisation?email_work_card=title
* https://www.academia.edu/17380088/Designing_Bodiless_Musical_Instruments?email_work_card=interaction_paper

## License

GNU GPLv3
